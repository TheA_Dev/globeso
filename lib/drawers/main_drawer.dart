import 'package:flutter/material.dart';
import 'package:globe_so/pages/main_page.dart';
import 'package:google_fonts/google_fonts.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

final numStyle = GoogleFonts.lato(
  color: Colors.white,
  fontSize: 15,
  letterSpacing: 0.80,
  fontWeight: FontWeight.w500,
);
final TileStyle = GoogleFonts.lato(
    color: Colors.blueGrey,
    fontSize: 20
);
class _MainDrawerState extends State<MainDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("assets/accss/bg-dw.jpg"))),
        child: ListView(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          children: [
            __Build_DrawerHeader(),
            //Home
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                  width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/home.png"),
              ),
              title: Text("Home",style: TileStyle,),
            ),
            //My Order
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                  width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/cart.png"),
              ),
              title: Text("My Order",style: TileStyle,),
            ),
            //Account History
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                  width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/dollar.png"),
              ),
              title: Text("Account History",style: TileStyle,),
            ),
            //Boost History
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                  width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/share.png"),
              ),
              title: Text("Boost History",style: TileStyle,),
            ),
            //Notification
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                  width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/alert.png"),
              ),
              title: Text("Notification",style: TileStyle,),
            ),
            //Wallet
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                  width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/wallet.png"),
              ),
              title: Text("Wallet",style: TileStyle,),
            ),
            //Earn Money
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                  width: 35,
                //color: Colors.green,
                image: AssetImage("assets/icons/dollar2.png"),
              ),
              title: Text("Earn \$ 100",style: TileStyle,),
            ),
            //Application Preferences
            ListTile(
              onTap: (){},
              title:  Text("Application Preferences",style: GoogleFonts.lato(
                color: Colors.grey,fontSize: 15,fontWeight: FontWeight.w400,
              ),),
              trailing: Image(
                height: 30,
                  width: 30,
                  color: Colors.grey,
                  image: AssetImage("assets/icons/minimize.png"),
              )
            ),
            //Help Center
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/exclam.png"),
              ),
              title: Text("Help Center",style: TileStyle,),
            ),
            //Setting
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/setting.png"),
              ),
              title: Text("Setting",style: TileStyle,),
            ),
            //Language
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                width: 35,
                //color: Colors.green,
                image: AssetImage("assets/icons/lg.png"),
              ),
              title: Text("Language",style: TileStyle,),
              trailing: Image(
                height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/US.png"),
              ),
            ),
            //Refresh
            ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (_)=>MainPage()));
              },
              leading: Image(
                height: 30,
                width: 30,
                color: Colors.green,
                image: AssetImage("assets/icons/refresh.png"),
              ),
              title: Text("Refresh",style: TileStyle,),
            ),
            //Contact Us
            ListTile(
              onTap: (){},
              leading: Image(
                height: 35,
                width: 35,
                color: Colors.green,
                image: AssetImage("assets/icons/contact.png"),
              ),
              title: Text("Contact Us",style: TileStyle,),
            ),
            //Version
            ListTile(
                onTap: (){},
                title:  Text("Version 2.5.8",style: GoogleFonts.lato(
                  color: Colors.grey,fontSize: 15,fontWeight: FontWeight.w400,
                ),),
                trailing: Image(
                  height: 30,
                  width: 30,
                  color: Colors.grey,
                  image: AssetImage("assets/icons/minimize.png"),
                )
            ),
          ],
        ),
      ),
    );
  }

  Container __Build_DrawerHeader() {
    return Container(
            height: 140,
      color: Colors.green,
            child: Stack(
              children: [
                //Profile
                Positioned(
                  top: 40,
                  left: 6,
                  child: GestureDetector(
                    onTap: () {},
                    child: CircleAvatar(
                      radius: 30,
                      backgroundImage: AssetImage("assets/accss/me.JPG"),
                    ),
                  ),
                ),
                //Details Number
                Positioned(
                    top: 20,
                    left: 70,
                    child: Container(
                      height: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //Phone Number
                          Text('+85593550930', style: numStyle),
                          //Id
                          Text('#00364167', style: numStyle),
                          //Phone Number
                          Text('+85593550930', style: numStyle),
                          //Rank
                          Text('Normal',style: GoogleFonts.lato(
                            color: Colors.white,
                            fontSize: 15,fontWeight: FontWeight.w400
                          ),),
                          //update profile
                          InkWell(
                            onTap: (){},
                            splashColor: Colors.indigo,
                            child: Text('update profile, check shop order',style: GoogleFonts.lato(
                                color: Colors.white,
                                fontSize: 12,fontWeight: FontWeight.w400
                            ),),
                          ),
                        ],
                      ),
                    )),
                //Icon Arrow Next
                Positioned(
                    top: 50,
                    right: 0,
                    child: IconButton(
                      onPressed: () {},
                      color: Colors.white,
                      iconSize: 25,
                      splashColor: Colors.indigo,
                      icon: Icon(Icons.arrow_forward_ios),
                    ))
              ],
            ),
          );
  }
}

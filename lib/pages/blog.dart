import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:globe_so/class_flies.dart';
import 'package:google_fonts/google_fonts.dart';

class BlogPage extends StatefulWidget {
  @override
  _BlogPageState createState() => _BlogPageState();

}
final cmtStyle = GoogleFonts.lato(
  color: Colors.grey,
  fontSize: 15,
  fontWeight: FontWeight.w500,
);

final caption = "ដំណឹងជ្រើសរើស ប្រធានក្រុមគណនេយ្យករ"
    "• ការងារទូទៅផ្នែកគណនេយ្យក្រុមហ៊ុន"
    "• ការងារគ្រប់គ្រងចំណូលចំណេញ"
    "• ការងារប្រកាសពន្ធ"
    "អត្ថប្រយោជន៌"
    "១ ធ្វើការងារ ៥ថ្ងៃកន្លះ  និងថ្ងៃបុណ្យសម្រាកតាមក្រសួងការងារ"
    "២ ប្រាក់ខែចរចាតាមសមត្ថភាពជាក់ស្តែង"
    "៣ ប្រាក់រង្វាន់ប្រចាំឆ្នាំ"
    "៤ ប្រាក់រង្វាន់អាំងបាំវ"
    "៥ បណ្តុះបណ្តាលក្លាយជាប្រធានគណនេយ្យករ គ្រប់គ្រងសាខាទូទាំងប្រទេស"
    "ដាក់ពាក្យ Telegram 0968818024";
class _BlogPageState extends State<BlogPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
       // shrinkWrap: true,
        slivers: [
          _GlobeSO_Super_App(context), _GlobeSO_Super_App(context), _GlobeSO_Super_App(context),
          _GlobeSO_Super_App(context), _GlobeSO_Super_App(context), _GlobeSO_Super_App(context),
          _GlobeSO_Super_App(context), _GlobeSO_Super_App(context), _GlobeSO_Super_App(context),
        ],
      ),
    );
  }

  SliverToBoxAdapter _GlobeSO_Super_App(BuildContext context) {
    return SliverToBoxAdapter(
          child: Container(
            height: 640,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              border:Border.symmetric(horizontal: BorderSide(color: Colors.grey.shade500,width: 2))
            ),
            child: Stack(
              children: [
                //AppBar
                Container(
                  height: 70,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: [
                      //Image
                      Positioned(
                        left: 20,
                         child: Image(
                          height: 65,
                          width: 65,
                          image: AssetImage("assets/accss/logo-.png"),
                        ),
                      ),
                      //name
                      Positioned(
                        top: 10,left: 100,
                        child: Text("GlobeSO Super App",style: GoogleFonts.lato(
                          color: Colors.green,
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                        ),),
                      ),
                      //date
                      Positioned(
                        bottom: 10,left: 100,
                        child: Text("2 months ago",style: GoogleFonts.lato(
                            color: Colors.blueGrey,
                            fontSize: 13,
                            fontWeight: FontWeight.w400
                        ),),
                      ),
                      //other
                      Positioned(
                        top: 10,
                          right: 0,
                          child: IconButton(
                              onPressed: (){},
                              color: Colors.black,
                              iconSize: 33,
                              icon: Icon(Icons.more_horiz)))
                    ],
                  ),
                ),
                //Image
                Positioned(
                    top:60,
                    child: Image(
                      height: 300,
                      width: MediaQuery.of(context).size.width,
                  image: AssetImage("assets/products/temple.jpg"),
                )),
                //Lover share cmt save
                Positioned(
                    top: 350,
                    child: Container(
                    height: 80,
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                    children: [
                      //Lover
                      Positioned(
                        left: 10,
                        top: 10,
                        child: Column(
                          children: [
                            InkWell(
                                onTap:(){},
                                child: Image(
                                    height: 30,
                                    width: 30,
                                    color: Colors.black,
                                    image: AssetImage('assets/icons/fav.png'))),
                            SizedBox(height: 8,),
                            Text("195",style: cmtStyle,),
                          ],
                        ),
                      ),
                      //Comment
                      Positioned(
                        left: 75,
                        top: 10,
                        child: Row(
                          children: [
                            InkWell(
                                onTap:(){},
                                child: Image(
                                    height: 30,
                                    width: 30,
                                    //color: Colors.white,
                                    image: AssetImage('assets/icons/cmt.png'))),
                            SizedBox(width: 8,),
                            Text("10",style: cmtStyle)
                          ],
                        ),
                      ),
                      //Share
                      Positioned(
                        left: 170,
                        top: 10,
                        child: Row(
                          children: [
                            InkWell(
                                onTap:(){},
                                child: Image(
                                    height: 30,
                                    width: 30,
                                    //color: Colors.white,
                                    image: AssetImage('assets/icons/share1.png'))),
                            SizedBox(width: 8,),
                            Text("10",style: cmtStyle)
                          ],
                        ),
                      ),
                      //Save
                      Positioned(
                          top: 10,
                          right: 5,
                          child: InkWell(
                        onTap: (){},
                        child: Image(
                          height: 30,
                          width: 30,
                          color: Colors.black,
                          image: AssetImage("assets/icons/save.png"),
                        ),
                      ))
                    ],
                  ),
                )),
                //Caption
                Positioned(
                    bottom: 0,
                    child: Container(
                      padding: EdgeInsets.all(8),
                        width: MediaQuery.of(context).size.width,
                        child: Text(caption,style: GoogleFonts.lato(
                          color: Colors.black,
                          fontSize: 18,
                        ),)))
              ],
            ),
          ),
        );
  }
}

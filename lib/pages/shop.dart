import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ShopPage extends StatefulWidget {
  @override
  _ShopPageState createState() => _ShopPageState();
}

class _ShopPageState extends State<ShopPage> {
  final Style_ = GoogleFonts.lato(
    color: Colors.grey, fontWeight: FontWeight.bold,
    fontSize: 15,
    //fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
      //shrinkWrap: true,
      physics: BouncingScrollPhysics(),
      slivers: [
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
        __Build_products(),
      ],
    ));
  }

  SliverToBoxAdapter __Build_products() {
    return SliverToBoxAdapter(
      child: InkWell(
        onTap: () {},
        splashColor: Colors.indigo.shade50,
        child: Container(
          height: 325,
          decoration: BoxDecoration(
            border: Border.symmetric(
                horizontal:
                    BorderSide(color: Colors.grey.shade300, width: 2)),
          ),
          child: Stack(
            children: [
              //Image
              Container(
                margin: EdgeInsets.all(8),
                height: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("assets/products/fd-1.jpg"),
                  ),
                ),
              ),
              //Logo
              Positioned(
                right: 25,
                bottom: 90,
                child: Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/products/logo-f.jpg"),
                      )),
                ),
              ),
              //name
              Positioned(
                  bottom: 75,
                  left: 5,
                  child: Text(
                    "គ្រឿងសមុទ្រក្រឡុកម្ទេស",
                    style: GoogleFonts.lato(
                      color: Colors.black,
                      fontSize: 20,
                      //fontWeight: FontWeight.w400,
                    ),
                  )),
              //Map km
              Positioned(
                  left: 5,
                  bottom: 10,
                  child: Container(
                    child: Row(
                      children: [
                        Image(
                          height: 25,
                          width: 25,
                          color: Colors.grey.shade900,
                          image: AssetImage("assets/icons/map.png"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "1.59 km",
                          style: Style_,
                        ),
                      ],
                    ),
                  )),
              //Time arrival
              Positioned(
                  left: 120,
                  bottom: 10,
                  child: Container(
                    child: Row(
                      children: [
                        Image(
                          height: 25,
                          width: 25,
                          color: Colors.grey.shade900,
                          image: AssetImage("assets/icons/time.png"),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "15 - 20 min",
                          style: Style_,
                        ),
                      ],
                    ),
                  )),
              //direction , Taxi , Chat
              Positioned(
                  right: 5,
                  bottom: 10,
                  child: Container(
                    // color: Colors.amber,
                    child: Row(
                      children: [
                        //Turn Right
                        Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              border: Border.all(
                                  color: Colors.green.shade500, width: 1.80)),
                          child: FittedBox(
                            fit: BoxFit.none,
                            child: Image(
                              height: 23,
                              width: 23,
                              color: Colors.grey.shade900,
                              image: AssetImage("assets/icons/tr.png"),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        //Taxi
                        InkWell(
                          onTap: () {},
                          borderRadius: BorderRadius.circular(20),
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                    color: Colors.green.shade500, width: 1.80)),
                            child: FittedBox(
                              fit: BoxFit.none,
                              child: Image(
                                height: 23,
                                width: 23,
                                color: Colors.grey.shade900,
                                image: AssetImage("assets/icons/taxi.png"),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        //Chat
                        InkWell(
                          onTap: () {},
                          borderRadius: BorderRadius.circular(20),
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                    color: Colors.green.shade500, width: 1.80)),
                            child: FittedBox(
                              fit: BoxFit.none,
                              child: Image(
                                height: 23,
                                width: 23,
                                color: Colors.grey.shade900,
                                image: AssetImage("assets/icons/sms.png"),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../class_flies.dart';

class Products_Page extends StatefulWidget {
  @override
  _Products_PageState createState() => _Products_PageState();
}
final Style =  GoogleFonts.lato(
    color: Colors.black54,
    fontSize: 16,
    fontWeight: FontWeight.w500);
class _Products_PageState extends State<Products_Page>
{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
     // shrinkWrap: true,
      physics: BouncingScrollPhysics(),
      slivers: [
        __Builld_Shampoo_Products(),
        __Builld_Shampoo_Products(),
      ],
    ));
  }

  SliverGrid __Builld_Shampoo_Products() {
    return SliverGrid(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 5,
          ),
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              return InkWell(
                onTap: (){},
                splashColor: Colors.indigo,
                borderRadius: BorderRadius.circular(10),
                child: Flexible(
                  //Use expanded or Flexible to show Radius
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.white60,
                          Colors.white70
                        ]
                      )
                    ),
                    child: Stack(
                      children: [
                        //Image
                        Container(
                          height: 150,
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/accss/bg2.jpg"))),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child:
                                Image(image: AssetImage(Products[index]['img'])),
                          ),
                        ),
                        //Name
                        Positioned(
                            bottom: 25,
                            left: 10,
                            child: Text(
                              Products[index]['name'],
                              style:Style)),
                        //Quantity
                        Positioned(
                            right: 5,
                            bottom: 25,
                            child: Text(Products[index]['quantity'],
                                style: Style)),
                        //Price
                        Positioned(
                          left: 10,
                            bottom: 0,
                            child: Text(
                              Products[index]['price'],
                              style: GoogleFonts.lato(
                                  color: Colors.black,
                                  letterSpacing: 1,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            )),
                        //New Arrival
                        Positioned(
                          right: 10,
                            bottom: 0,
                            child: InkWell(
                              onTap: (){},
                              splashColor: Colors.indigo,
                              borderRadius: BorderRadius.circular(5),
                              child: Text(
                                "New",
                                style: GoogleFonts.lato(
                                    color: Colors.green,
                                    decoration: TextDecoration.underline,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                            )),
                      ],
                    ),
                  ),
                ),
              );
            },
            childCount: Products.length,
            // addRepaintBoundaries: true,
            // addAutomaticKeepAlives: true,
            // addSemanticIndexes: true,
          ));
  }
}

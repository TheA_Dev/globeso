import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/cupertino.dart';
 import 'package:flutter/material.dart';
import 'package:globe_so/class_flies.dart';
import 'package:globe_so/drawers/main_drawer.dart';
import 'package:globe_so/drawers/main_end_drawer.dart';
import 'package:globe_so/pages/biz_page.dart';
import 'package:globe_so/pages/blog.dart';
import 'package:globe_so/pages/products.dart';
import 'package:globe_so/pages/shop.dart';
import 'package:globe_so/widgets/main_controller.dart';
import 'package:google_fonts/google_fonts.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  final styleGlobeSO = GoogleFonts.lato(
    color: Colors.white,
    fontSize: 23,
    fontWeight: FontWeight.w300,
  );
  final styleG = GoogleFonts.lato(
    color: Colors.white,
    fontSize: 15,
    fontWeight: FontWeight.w400,
  );
  final Style_SG = GoogleFonts.lato(
    color: Colors.blueGrey,
    fontSize: 10,
    //fontWeight: FontWeight.w400,
  );

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  final List<Tab> CetegoriesTap = <Tab>[
    Tab(
      icon: Text("Shop"),
    ),
    Tab(
      child: Text("Biz Page"),
    ),
    Tab(
      child: Text("Products"),
    ),
    Tab(
      child: Text("Blog"),
    ),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: CetegoriesTap.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      drawer: MainDrawer(),
      drawerScrimColor: Colors.white54,
      drawerEdgeDragWidth: 40,
      endDrawer: MainEndDrawer(),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover, image: AssetImage("assets/accss/bg.jpg"))),
        child: Column(
          children: [
            //AppBar
            __BuildAppBar(context),
            //CustomScrollView
            Flexible(
              child: CustomScrollView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: BouncingScrollPhysics(),
                slivers: [
                  //Swiper
                  SliverToBoxAdapter(
                    child: __BuildSlider(),
                  ),
                  //Categories pageView
                  SliverToBoxAdapter(
                      child: ___BuildCategoriesPageViews(context)),
                  // Latest Products
                  SliverToBoxAdapter(child: __Build_LatestProducts()),
                  //All Products
                  SliverToBoxAdapter(
                    child: __Build_products_categories(context),
                  ),
                ],
              ),
            ),
            //BottomNavigationBar
            __BuildBottomBar()
          ],
        ),
      ),
    );
  }

  Flexible __Build_products_categories(BuildContext context) {
    return Flexible(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(top: 10),
        child: NestedScrollView(
          physics: BouncingScrollPhysics(),
          headerSliverBuilder: (context, bool innerBoxIsScrolled) {
            return [
              SliverOverlapAbsorber(
                  handle:
                      NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: SliverToBoxAdapter(
                    child: Expanded(
                     child: Row(
                       children: [
                         Flexible(
                           child: Container(
                             height: 70,
                             color: Colors.white,
                             child: TabBar(
                               controller: _tabController,
                                tabs: CetegoriesTap,
                               //isScrollable: true, //We use Row so don't need it
                               indicatorColor: Colors.black,
                               indicatorWeight:2,
                               indicatorSize: TabBarIndicatorSize.label,
                               unselectedLabelColor: Colors.grey,
                               labelColor: Colors.black,
                               labelStyle: GoogleFonts.lato(
                                  fontSize: 16,
                                 fontWeight: FontWeight.w400,
                               ),
                               unselectedLabelStyle: GoogleFonts.lato(
                                  fontSize: 13,
                                 fontWeight: FontWeight.w400
                                 //fontWeight: FontWeight.w400,
                               ),
                             ),
                           ),
                         ),
                       ],
                     ),
                      ),
                  )),
            ];
          },
          body: TabBarView(
            physics: BouncingScrollPhysics(),
            controller: _tabController,
            children: [
              ShopPage(),
              Biz_Page(),
              Products_Page(),
              BlogPage(),
            ],
          ),
        ),
      ),
    );
  }

  Container __BuildSlider() {
    return Container(
      height: 190,
      child: Swiper(
        autoplay: true,
        curve: Curves.fastLinearToSlowEaseIn,
        itemCount: Swip.length,
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(13),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(Swip[index]['img']),
                )),
          );
        },
      ),
    );
  }

  Container __Build_LatestProducts() {
    return Container(
      margin: EdgeInsets.only(left: 5, right: 5),
      height: 280,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(7)),
      child: Column(
        children: [
          //Rocket ...
          GestureDetector(
            onTap: () {},
            child: Container(
              height: 76,
              child: Stack(
                children: [
                  //Rocket
                  Positioned(
                      left: 13,
                      top: 8,
                      child: CircleAvatar(
                        radius: 17,
                        backgroundColor: Colors.deepOrange,
                        child: Image(
                          height: 29,
                          color: Colors.white,
                          image: AssetImage("assets/icons/rocket.png"),
                        ),
                      )),
                  //Latest Products
                  Positioned(
                    left: 55,
                    top: 13,
                    child: Text("Latest Products",
                        style: GoogleFonts.lato(
                          color: Colors.green,
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                        )),
                  ),
                  //discount
                  Positioned(
                    left: 10,
                    bottom: 0,
                    child: Text("Hot now at up to 90% off",
                        style: GoogleFonts.lato(
                          fontSize: 18,
                          color: Colors.grey,
                        )),
                  ),
                  //More
                  Positioned(
                    right: 10,
                    top: 13,
                    child: Text(
                      "More",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: Products_Slider.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      Container(
                        margin: EdgeInsets.all(8),
                        height: 140,
                        width: 140,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.indigo.shade200,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(Products_Slider[index]['img']),
                          ),
                        ),
                      ),
                      Text(Products_Slider[index]['name']),
                      Text(Products_Slider[index]['type']),
                    ],
                  );
                }),
          )
        ],
      ),
    );
  }

  Container ___BuildCategoriesPageViews(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 25),
      height: 430,
      width: MediaQuery.of(context).size.width,
      child: PageView.custom(
          physics: BouncingScrollPhysics(),
          reverse: true,
          childrenDelegate: SliverChildListDelegate([
            //First PageViev
            FirstPageView(),
            //Second PageView
            SecondPageView(),
          ])),
    );
  }

  Container SecondPageView() {
    return Container(
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          //First Row
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //G-Ride
                Container(
                  child: Column(
                    children: [
                      Container(
                        //  margin: EdgeInsets.only(left: 20),
                        height: 170, width: 170,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image: AssetImage("assets/categ/G-ride.png"))),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                          'G-Ride',
                          style: styleG,
                        ),
                      ),
                    ],
                  ),
                ),
                //G-Express
                Container(
                  child: Column(
                    children: [
                      Container(
                        // margin: EdgeInsets.only(right: 20),
                        height: 170, width: 170,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image:
                                    AssetImage("assets/categ/G-express.png"))),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                          'G-Express',
                          style: styleG,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          //Second Row
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //G-Market
                Container(
                  child: Column(
                    children: [
                      Container(
                        //  margin: EdgeInsets.only(left: 20),
                        height: 170, width: 170,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image:
                                    AssetImage("assets/categ/G-market.png"))),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                          'G-Market',
                          style: styleG,
                        ),
                      ),
                    ],
                  ),
                ),
                //G-Shop
                Container(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        height: 170,
                        width: 170,
                        decoration: BoxDecoration(
                          // color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Stack(
                          children: [
                            //Shop
                            Container(
                              height: 75,
                              width: 75,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.white,
                                  image: DecorationImage(
                                    alignment: Alignment.topCenter,
                                    fit: BoxFit.cover,
                                    image: AssetImage("assets/categ/shop.png"),
                                  )),
                            ),
                            //Restaurants
                            Positioned(
                              right: 0,
                              child: Container(
                                height: 75,
                                width: 75,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.white,
                                    image: DecorationImage(
                                      alignment: Alignment.topCenter,
                                      fit: BoxFit.cover,
                                      image:
                                          AssetImage("assets/categ/food.png"),
                                    )),
                              ),
                            ),
                            //Grocery Store
                            Positioned(
                                left: 0,
                                bottom: 0,
                                child: Container(
                                  height: 75,
                                  width: 75,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: Colors.white,
                                      image: DecorationImage(
                                        alignment: Alignment.topCenter,
                                        fit: BoxFit.cover,
                                        image: AssetImage(
                                            "assets/categ/store.png"),
                                      )),
                                )),
                            //Add More
                            Positioned(
                                right: 0,
                                bottom: 0,
                                child: Container(
                                  height: 75,
                                  width: 75,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.white,
                                  ),
                                  child: IconButton(
                                    alignment: Alignment.center,
                                    iconSize: 50,
                                    splashColor: Colors.indigo,
                                    color: Colors.black,
                                    onPressed: () {},
                                    icon: Icon(Icons.add_rounded),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                          'Suggestion',
                          style: styleG,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container FirstPageView() {
    return Container(
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          //First Row
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //G-Page
                Container(
                  child: Column(
                    children: [
                      Container(
                        //  margin: EdgeInsets.only(left: 20),
                        height: 170, width: 170,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image: AssetImage("assets/categ/G-page.png"))),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                          'G-Page',
                          style: styleG,
                        ),
                      ),
                    ],
                  ),
                ),
                //G-Social
                Container(
                  child: Column(
                    children: [
                      Container(
                        // margin: EdgeInsets.only(right: 20),
                        height: 170, width: 170,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image:
                                    AssetImage("assets/categ/G-socail.png"))),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                          'G-Social',
                          style: styleG,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          //Second Row
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //Suggestion
                Container(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(1.50),
                        //  margin: EdgeInsets.only(left: 20),
                        height: 170,
                        width: 170,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Stack(
                          children: [
                            //Hire Friend
                            Container(
                              height: 70,
                              width: 70,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                alignment: Alignment.topCenter,
                                fit: BoxFit.cover,
                                image: AssetImage("assets/categ/hire fri.png"),
                              )),
                            ),
                            //Text
                            Positioned(
                              top: 65,
                              left: 9,
                              child: Text(
                                "Hire Friend",
                                style: Style_SG,
                              ),
                            ),
                            //Laundry
                            Positioned(
                              right: 0,
                              child: Container(
                                height: 70,
                                alignment: Alignment.center,
                                width: 70,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  alignment: Alignment.topCenter,
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/categ/laundry.png"),
                                )),
                              ),
                            ),
                            //Text
                            Positioned(
                              top: 65,
                              right: 16,
                              child: Text(
                                "Laundry",
                                style: Style_SG,
                              ),
                            ),
                            //Map
                            Positioned(
                                left: 0,
                                bottom: 14,
                                child: Container(
                                  height: 70,
                                  width: 70,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                    alignment: Alignment.topCenter,
                                    fit: BoxFit.cover,
                                    image: AssetImage("assets/categ/map.png"),
                                  )),
                                )),
                            //Text
                            Positioned(
                              bottom: 4,
                              left: 26,
                              child: Text(
                                "Map",
                                style: Style_SG,
                              ),
                            ),
                            //Chat
                            Positioned(
                                right: 0,
                                bottom: 14,
                                child: Container(
                                  height: 70,
                                  width: 70,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                    alignment: Alignment.topCenter,
                                    fit: BoxFit.cover,
                                    image: AssetImage("assets/categ/chat.png"),
                                  )),
                                )),
                            //Text
                            Positioned(
                              bottom: 4,
                              right: 21,
                              child: Text(
                                "Chat",
                                style: Style_SG,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                          'Suggestion',
                          style: styleG,
                        ),
                      ),
                    ],
                  ),
                ),
                //G-Service
                Container(
                  child: Column(
                    children: [
                      Container(
                        // margin: EdgeInsets.only(right: 20),
                        height: 170, width: 170,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image:
                                    AssetImage("assets/categ/G-service.png"))),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                          'G-Service',
                          style: styleG,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container __BuildBottomBar() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 20, right: 20),
      height: 85,
      width: 400,
      decoration: BoxDecoration(
        color: Colors.black54,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //Taxi
          Card(
              shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(2),
                  side: BorderSide(color: Colors.white, width: 0.50)),
              color: Colors.black12,
              child: IconButton(
                  splashColor: Colors.indigo,
                  color: Colors.white,
                  tooltip: "Taxi",
                  iconSize: 40,
                  icon: Icon(Icons.local_taxi),
                  onPressed: () {})),
          //Cart
          Card(
              shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(2),
                  side: BorderSide(color: Colors.white, width: 0.50)),
              color: Colors.black12,
              child: IconButton(
                  splashColor: Colors.indigo,
                  color: Colors.white,
                  tooltip: "Cart",
                  iconSize: 40,
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () {})),
          //Favorite
          Card(
              shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(2),
                  side: BorderSide(color: Colors.white, width: 0.50)),
              color: Colors.black12,
              child: IconButton(
                  splashColor: Colors.indigo,
                  color: Colors.white,
                  tooltip: "Favorite",
                  iconSize: 40,
                  icon: Icon(Icons.favorite),
                  onPressed: () {})),
          //History
          Card(
              shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(2),
                  side: BorderSide(color: Colors.white, width: 0.50)),
              color: Colors.black12,
              child: IconButton(
                  splashColor: Colors.indigo,
                  color: Colors.white,
                  tooltip: "History",
                  iconSize: 40,
                  icon: Icon(Icons.my_library_books_sharp),
                  onPressed: () {})),
          //Profile
          Card(
              shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(2),
                  side: BorderSide(color: Colors.white, width: 0.50)),
              color: Colors.black12,
              child: IconButton(
                  splashColor: Colors.indigo,
                  color: Colors.white,
                  tooltip: "Profile",
                  iconSize: 40,
                  icon: Icon(Icons.person_pin_outlined),
                  onPressed: () {
                    _globalKey.currentState.openEndDrawer();
                  })),
        ],
      ),
    );
  }

  Container __BuildAppBar(BuildContext context) {
    return Container(
      //color: Colors.indigo,
      padding: EdgeInsets.only(top: 40),
      alignment: Alignment.center,
      height: 100,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          //Drawer
          Positioned(
            left: 5,
            child: TextButton(
                onPressed: () {
                  _globalKey.currentState.openDrawer();
                },
                child: Image(
                  height: 30,
                  width: 30,
                  color: Colors.white,
                  fit: BoxFit.cover,
                  image: AssetImage("assets/icons/menu.png"),
                )),
          ),
          //GlobeSO
          Positioned(
              left: 65,
              child: TextButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => MainPage()));
                },
                child: Text(
                  "GlobeSO",
                  style: styleGlobeSO,
                ),
              )),
          // QR Code
          Positioned(
            right: 150,
            child: TextButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => MainController()));
              },
              child: Image(
                alignment: Alignment.center,
                height: 35,
                width: 35,
                color: Colors.white,
                fit: BoxFit.cover,
                image: AssetImage("assets/icons/qr_code.png"),
              ),
            ),
          ),
          //Search
          Positioned(
              right: 85,
              child: TextButton(
                onPressed: () {},
                child: Image(
                  height: 30,
                  width: 30,
                  color: Colors.white,
                  fit: BoxFit.cover,
                  image: AssetImage("assets/icons/search.png"),
                ),
              )),
          //Location
          Positioned(
              right: 48,
              child: TextButton(
                onPressed: () {},
                child: Image(
                  height: 35,
                  width: 30,
                  color: Colors.white,
                  fit: BoxFit.cover,
                  image: AssetImage("assets/icons/map.png"),
                ),
              )),
          //Profile
          Positioned(
              right: 5,
              child: GestureDetector(
                onTap: () {
                  _globalKey.currentState.openEndDrawer();
                },
                child: CircleAvatar(
                  radius: 25,
                  backgroundImage: AssetImage("assets/accss/me.JPG"),
                ),
              )),
        ],
      ),
    );
  }
}

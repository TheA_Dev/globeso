import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Biz_Page extends StatefulWidget {
  @override
  _Biz_PageState createState() => _Biz_PageState();
}

final Style_ = GoogleFonts.lato(
  color: Colors.grey,
  fontSize: 15,
);

class _Biz_PageState extends State<Biz_Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
      physics: BouncingScrollPhysics(),
    //  shrinkWrap: true,
      slivers: [
        ___BuildProducts(context),___BuildProducts(context),___BuildProducts(context),
        ___BuildProducts(context),___BuildProducts(context),___BuildProducts(context),
        ___BuildProducts(context),___BuildProducts(context),___BuildProducts(context),
        ___BuildProducts(context),___BuildProducts(context),___BuildProducts(context),
        ___BuildProducts(context),___BuildProducts(context),___BuildProducts(context),
      ],
    ));
  }

  SliverToBoxAdapter ___BuildProducts(BuildContext context) {
    return SliverToBoxAdapter(
        child: InkWell(
          onTap: () {},
          child: Container(
            height: 380,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.symmetric(
                    horizontal:
                        BorderSide(color: Colors.grey.shade200, width: 3))),
             child: Stack(
              children: [
                //Top Appbar
                Container(
                  height: 70,
                  //color: Colors.indigo,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      CircleAvatar(
                        radius: 30,
                        backgroundImage:
                            AssetImage("assets/products/logo-j.jpg"),
                        child: Stack(
                          children: [
                            Positioned(
                                top: 0,
                                right: 0,
                                child: Icon(
                                  Icons.verified_user,
                                  color: Colors.green,
                                ))
                          ],
                        ),
                      ),
                      Text(
                        "Junie shop",
                        style: GoogleFonts.lato(
                          fontSize: 18,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(
                        width: 190,
                      ),
                      IconButton(
                          onPressed: () {},
                          iconSize: 25,
                          color: Colors.green,
                          icon: Icon(Icons.favorite)),
                    ],
                  ),
                ),
                //ddd
                Positioned(
                    top: 50,
                    child: Divider(height: 20,
                  color: Colors.black,
                    thickness: 20,
                )),
                //Image
                Positioned(
                    top: 80,
                    child: Container(
                      height: 200,
                      width: 400,
                      // Need to set width and height
                      margin: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage("assets/products/jn.jpg"))),
                    )),
                //Location Street
                Positioned(
                    bottom: 60,
                    left: 20,
                    child: Text(
                      "34A, St.70, Phnom Penh",
                      style: Style_,
                    )),
                //More Details
                Positioned(
                    bottom: 0,
                    left: 20,
                    right: 20,
                    child: Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Viewers
                            Container(
                              height: 27,
                              width: 75,
                              decoration: BoxDecoration(
                                color: Colors.grey.shade300,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Icon(
                                    Icons.remove_red_eye,
                                    color: Colors.grey,
                                  ),
                                  Text(
                                    "95.4k",
                                    style: Style_,
                                  ),
                                ],
                              ),
                            ),
                            //Clickers
                            Container(
                              height: 27,
                              //width: 75,
                              padding: EdgeInsets.all(5),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Colors.grey.shade300,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Text(
                                "6.7k Clicks",
                                style: Style_,
                              ),
                            ),
                            //Direction , Call , sms....
                            Container(
                              height: 60,
                              width: 180,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  //Call
                                  InkWell(
                                    onTap: () {},
                                    child: Image(
                                      height: 20,
                                      width: 20,
                                      color: Colors.grey,
                                      image:
                                          AssetImage("assets/icons/call.png"),
                                    ),
                                  ),
                                  //Direction
                                  Image(
                                    height: 20,
                                    width: 20,
                                    color: Colors.grey,
                                    image: AssetImage("assets/icons/tr.png"),
                                  ),
                                  //Taxi
                                  InkWell(
                                    onTap: () {},
                                    borderRadius: BorderRadius.circular(20),
                                    child: Image(
                                      height: 20,
                                      width: 20,
                                      color: Colors.grey,
                                      image:
                                          AssetImage("assets/icons/taxi.png"),
                                    ),
                                  ),
                                  //Chat
                                  InkWell(
                                    onTap: () {},
                                    borderRadius: BorderRadius.circular(20),
                                    child: Image(
                                      height: 20,
                                      width: 20,
                                      color: Colors.grey,
                                      image:
                                          AssetImage("assets/icons/sms.png"),
                                    ),
                                  ),
                                  //sms
                                  InkWell(
                                    onTap: () {},
                                    borderRadius: BorderRadius.circular(20),
                                    child: Image(
                                      height: 20,
                                      width: 20,
                                      color: Colors.grey,
                                      image:
                                          AssetImage("assets/icons/sms1.png"),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ))),
              ],
            ),
          ),
        ),
      );
  }
}

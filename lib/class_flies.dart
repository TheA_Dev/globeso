import 'package:flutter/material.dart';

final Swip = [
  {
    "img": "assets/swiper/swip1.png",
  },
  {
    "img": "assets/swiper/swip2.png",
  },
  {
    "img": "assets/swiper/swip3.png",
  },
];

final Products_Slider = [
  {
    "img": "assets/products/p1.jpg",
    "name": "GGLORIUS Premium",
    "type": "Hair Set",
  },
  {
    "img": "assets/products/p2.png",
    "name": "Kopher Serum Black",
    "type": "Edition",
  },
  {
    "img": "assets/products/p3.jpg",
    "name": "Kopher Claiming",
    "type": "Ocean Suncream",
  },
  {
    "img": "assets/products/p4.jpg",
    "name": "Kopher Relief",
    "type": "Concentrate",
  },
  {
    "img": "assets/products/p5.png",
    "name": "Hydrating Facial",
    "type": "Tonic",
  },
  {
    "img": "assets/products/p6.png",
    "name": "Liposomal Face",
    "type": "Serum",
  },
];
final Products = [
  {
    "img": "assets/products/hema4.png",
    "name": "សាប៊ូដុំ ហេម៉ា​5002",
    "quantity": "(80 ក្រាម)",
    "price": "\$ 0.90",
  },
  {
    "img": "assets/products/hema2.png",
    "name": "សាប៊ូម្សៅ ហេម៉ា​5002",
    "quantity": "(80 ក្រាម)",
    "price": "\$ 0.90",
  },
  {
    "img": "assets/products/hema2.png",
    "name": "សាប៊ូម្សៅ ហេម៉ា​4003",
    "quantity": "(3 គីឡូ)",
    "price": "\$ 5.00",
  },
  {
    "img": "assets/products/hema1.png",
    "name": "ថ្នាំលាងម៉ាស៊ីន​6018",
    "quantity": "(1 លីត្រ)",
    "price": "\$ 5.80",
  },
  {
    "img": "assets/products/hema3.png",
    "name": "ថ្នាំលាងម៉ាស៊ីន​6018",
    "quantity": "(1 លីត្រ)",
    "price": "\$ 10.50",
  },
  {
    "img": "assets/products/hema5.png",
    "name": "ថ្នាំលាងម៉ាស៊ីន​6018",
    "quantity": "(1 លីត្រ)",
    "price": "\$ 5.80",
  },
];

import 'package:flutter/material.dart';
import 'package:globe_so/pages/biz_page.dart';
import 'package:globe_so/pages/blog.dart';
import 'package:globe_so/pages/products.dart';
import 'package:globe_so/pages/shop.dart';

class MainController extends StatefulWidget {
  const MainController({Key key}) : super(key: key);

  @override
  _MainControllerState createState() => _MainControllerState();
}

class _MainControllerState extends State<MainController>with SingleTickerProviderStateMixin {
  final List<Tab> CetegoriesTap = <Tab>[
    Tab(
      child: Text("Shop"),
    ),
    Tab(
      child: Text("Biz Page"),
    ),
    Tab(
      child: Text("Products"),
    ),
    Tab(
      child: Text("Blog"),
    ),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: CetegoriesTap.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: NestedScrollView(
          headerSliverBuilder: (context, bool innerBoxIsScrolled) {
            return [
              SliverOverlapAbsorber(
                  handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: SliverAppBar(
                    pinned: true,
                    bottom: TabBar(
                      controller: _tabController,
                      tabs:  CetegoriesTap,
                    ),
                  )
              ),
            ];
          },
          body: TabBarView(
            controller: _tabController,
            children: [
               ShopPage(),
              Biz_Page(),
              Products_Page(),
              BlogPage(),
            ],
          ),
        ),
      ),
    );
  }
}
